TARGET := iphone:clang:latest:7.0
INSTALL_TARGET_PROCESSES = SpringBoard


include $(THEOS)/makefiles/common.mk

TWEAK_NAME = GboardFixer

GboardFixer_FILES = Tweak.x
GboardFixer_CFLAGS = -fobjc-arc

include $(THEOS_MAKE_PATH)/tweak.mk

THEOS_DEVICE_IP = 10.0.0.111
